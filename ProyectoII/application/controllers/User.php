<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function login()
    {
        // params
        // load data
        $this->load->view('user/login');
    }

    public function logout()
    {
        $this->session->sess_destroy();
        $this->session->set_flashdata('error', 'Inicie sesión nuevamente');
        redirect(site_url(['user', 'login']));
    }

    public function dashboard()
    {
        if ($this->session->has_userdata('user')) {
            $this->load->view('user/dashboard');
        } else {
            $this->session->set_flashdata('error', 'No ha iniciado sesión');
            redirect(site_url(['user', 'login']));
        }
    }

    public function registro()
    {
        $this->load->view('user/registro');
    }

    public function index()
    {
        $this->load->view('user/index');
    }

    public function registrarUsuario()
    {
        $validation_errors = [];

        if ($filename = $this->user_model->uploadPicture('picture')) {
            if (!$this->input->post('nombre')) {
                $validation_errors['nombre'] = 'is blank';
            }
            if (!$this->input->post('apellido')) {
                $validation_errors['apellido'] = 'is blank';
            }
            if (!$this->input->post('pais')) {
                $validation_errors['pais'] = 'is blank';
            }
            if (!$this->input->post('telefono')) {
                $validation_errors['telefono'] = 'is blank';
            }
            if (!$this->input->post('direccion')) {
                $validation_errors['direccion'] = 'is blank';
            }
            if (!$this->input->post('correo')) {
                $validation_errors['correo'] = 'is blank';
            }
            if (!$this->input->post('contrasena')) {
                $validation_errors['contrasena'] = 'is blank';
            }
            if (sizeof($validation_errors) > 0) {
                $this->session->set_flashdata('validation_errors', $validation_errors);
                redirect(site_url(['user', 'register']));
            } else {
            }
        } else {
            echo "Ha ocurrido un error al seleccionar la imagen";
        }
    }

    public function search($name = '')
    {
        $data['users'] = $this->user_model->getByName($name);
        $this->load->view('user/list', $data);
    }

    public function list()
    {
        $data['users'] = $this->user_model->all();
        $this->load->view('user/list', $data);
    }

    /**
     * Authenticates a user
     */
    public function authenticate()
    {
        // get username and password
        $pass = $this->input->post('password');
        $user = $this->input->post('username');

        // check the database with that information
        $authUser = $this->user_model->authenticate($user, $pass);
        // return error or redirect to landing page
        if ($authUser) {
            $this->session->set_userdata('user', $authUser);
            redirect(site_url(['user', 'dashboard']));
        } else {
            $this->session->set_flashdata('error', 'Invalid user name or password');
            redirect(site_url(['user', 'login']));
        }
    }

    public function validateRegister()
    {
        if ($filename = $this->user_model->uploadPicture('picture')) {
            $nombre = $this->input->post('nombre');
            $apellido = $this->input->post('apellido');
            $pais = $this->input->post('pais');
            $telefono = $this->input->post('telefono');
            $direccion = $this->input->post('direccion');
            $correo = $this->input->post('correo');
            $contrasena = $this->input->post('contrasena');
            $foto = $filename;
            $tipo = "usuario";
            if (empty($nombre) || empty($apellido) || empty($pais) || empty($telefono) || empty($direccion) || empty($correo)
            || empty($contrasena) || empty($foto)) {
                echo "Debe rellenar todos los espacios";
            } else {
                // check the database with that information
                $register = $this->user_model->saveUser($nombre, $apellido, $pais, $telefono, $direccion, $correo, $contrasena, $tipo);
                // return error or redirect to landing page
                if ($register) {
                    redirect(site_url(['user','index']));
                    //echo "Ha ocurrido un error al registrarse";
                } else {
                    //$this->session->set_flashdata('error', 'Invalid user name or password');
                    //redirect(site_url(['user','register']));
                    echo "Se ha registrado correctamente";
                }
            }
        }
    }
}
