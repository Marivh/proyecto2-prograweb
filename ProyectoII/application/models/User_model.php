<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User_model extends CI_Model
{

    /**
     *  Valida si un usuario existe en la base de datos
     *
     * @param $username  The username
     * @param $password The user's password
     */
    public function authenticate($username, $password)
    {
        $query = $this->db->get_where('usuario', array('correo' => $username, 'contrasena' => $password));
        if ($query->result()) {
            return $query->result()[0];
        } else {
            return false;
        }
    }

    /**
     *  Registra un usuario en la base de datos
     *
     * @param $nombre  Nombre del usuario
     * @param $apellido Apellido del usuario
     * @param $pais Pais de donde vive el usuario
     * @param $telefono Telefono del usuario
     * @param $direccion Direccion del usuario
     * @param $correo Correo del usuario
     * @param $contrasena Contrasena del usuario
     * @param $foto Foto del usuario
     */
    public function saveUser($nombre, $apellido, $pais, $telefono, $direccion, $correo, $contrasena, $foto, $tipo)
    {
        $data  =  array(
            'nombre'  =>  $nombre,
            'apellido'  =>  $apellido,
            'pais'  =>  $pais,
            'telefono'  =>  $telefono,
            'direccion'  =>  $direccion,
            'correo'  =>  $correo,
            'contrasena'  =>  $contrasena,
            'tipo'  =>  $tipo,
            'foto'  =>  $foto,
        );
        $query = $this->db->insert('usuario',  $data);
    }

    /**
     *  Obtiene los usuarios por ID
     *
     * @param $id  El ID del usuario
     */
    public function getUser($id)
    {
        $query = $this->db->get_where('usuario', array('id' => $id));
        if ($query->result()) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     *  Obtiene todos los usuarios de la base de datos
     *
     */
    public function getUsers()
    {
        $query = $this->db->get('usuario');
        return $query->result();
    }

    /**
     * Uploads an image to the server
     *
     * @inputName name of the input that holds the image in the request
     */
    function uploadPicture($inputName)
    {
        $fileObject = $_FILES[$inputName];

        $target_dir = "update/";
        $target_file = $target_dir . basename($fileObject["name"]);
        $uploadOk = 0;
        if (move_uploaded_file($fileObject["tmp_name"], $target_file)) {
            return $target_file;
        } else {
            return false;
        }
    }

    /**
     *  Hace una busqueda de los usuarios con las letras que el usuario vaya digitanto
     *
     * @param $busqueda  El nombre que el administrador va a ir digitando
     */
    public function obtenerAmigoPorNombre($busqueda)
    {
        $query = $this -> db -> like ( 'nombre' ,  $busqueda ); 
        if ($query->result()) {
            return $query->result();
        } else {
            return false;
        }
    }
}
