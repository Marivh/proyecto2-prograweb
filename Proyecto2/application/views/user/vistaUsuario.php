<?php
defined('BASEPATH') or exit('No direct script access allowed');
if($this->session->usuario->tipo !== "usuario"){
    redirect(site_url(['user', 'index']));
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/vistaUsuario.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
</head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<title>Document</title>
</head>

<body>
    <nav class="navbar" style="background-color: #000080;">
        <!-- Brand -->
        <a class="navbar-brand" href="#">
            <img src="img\Amigos de un millón de árboles.png" width="80" height="80" alt="">
        </a>
        <!-- Links -->
        <ul class="nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" data-toggle="collapse" href="#mis-arboles" aria-expanded="false" aria-controls="mis-arboles">Mis árboles</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="collapse" href="#arbol" aria-expanded="false" aria-controls="arbol">Comprar árbol</a>
            </li>
            <!--Ventana de pergfil-->
            <li class="nav-item dropdown navbar-text">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <span class="far fa-user"></span> 
                    <strong><?php echo $this->session->usuario->nombre; ?></strong>
                    <span class="glyphicon glyphicon-chevron-down"></span>
                </a>
                <div class="container">
                    <ul class="dropdown-menu ml-auto">
                        <li id="login">
                            <div class="navbar-login">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <p class="text-center">
                                            <span><img src="/WEB/proyecto2-prograweb/Proyecto2/<?php echo $this->session->usuario->foto; ?>" width="80" height="80"></img></span>
                                        </p>
                                    </div>
                                    <div class="col-lg-8">
                                        <p class="text-left"><strong><?php echo $this->session->usuario->nombre; ?></strong></p>
                                        <p class="text-left small"><?php echo $this->session->usuario->correo; ?></p>
                                        <p class="text-left">
                                            <a href="<?php echo site_url(['user', 'logout']); ?>" class="btn btn-primary btn-block btn-sm">Cerrar sesión</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <div class="navbar-login navbar-login-session">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <p>
                                            <a href="#" class="btn btn-primary btn-block">Mi perfil</a>
                                            <a href="#" class="btn btn-danger btn-block">Cambiar contraseña</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
    </nav>
    <!--Muestra los arboles comprados por este usuario-->
    <div class="collapse" id="mis-arboles">
        <div class="card card-body table-responsive">
            <h1 class="font-weight-light text-center text-lg-left mt-4 mb-0">Mis arboles</h1>
            <div class="row  h-100 justify-content-start">
                <div class="col-sm-12">
                    <div class="card justify-content-start">
                        <div class="card-body justify-content-start">
                            <?php
                            if ($mytrees !== FALSE) {
                                foreach ($mytrees as $tree) {
                                    echo "<img src ='/WEB/proyecto2-prograweb/Proyecto2/{$tree->foto}' width='150' height='150' >
                                    <h3>{$tree->nombre}</h3><p><a href=" . site_url('/arbol/detalleArbolUsuario/' . $tree->id_arbol) . ">Ver detalle</a></p>";

                                }
                            } else {
                                echo "No has adquirido ningún árbol";
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--Muestra los arboles disponibles para comprar-->
    <div class="collapse" id="arbol">
        <div class="card card-body h-100 justify-content-start">
            <form action="">
                <h3></h3>
            </form>
            <h1 class="font-weight-light text-center text-lg-left mt-4 mb-0">Galería de arboles</h1>

            <div class="row  h-100 justify-content-start">
                <div class="col-sm-12">
                    <div class="card justify-content-start">
                        <div class="card-body justify-content-start">
                            <?php
                            foreach ($arboles as $tree) {
                                echo "<img id_arbol='{$tree->id_arbol}' class='img-fluid' src ='/WEB/proyecto2-prograweb/Proyecto2/{$tree->foto}' width='150' height='150'>
                                        <h3>{$tree->tipo}</h3><p><a href=" . site_url('/arbol/detalleArbol/' . $tree->id_arbol) . ">Ver detalle</a></p>";
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>