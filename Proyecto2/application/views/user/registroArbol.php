<?php
defined('BASEPATH') or exit('No direct script access allowed');

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" type="text/css" href="registro.css">
  <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <script src="assets/js/actions.js"></script>
  <title>Document</title>
</head>

<body>
  <div class="container">
    <form  action="<?php echo site_url(['arbol', 'saveTree']) ?>" method="POST" class="form" role="form">
      <br>
      <br>
      <br>
      <div class="form-group col-md-6">
        <img src="img\cierreagr2.jpg" alt="">
      </div>
      <div class="form-group col-md-6">
        <h1>Registrar arbol</h1>
      </div>
      <div class="form-group col-md-6">
        <label class="sr-only" for="">Tipo</label>
        <input type="text" class="form-control" id="tipo" name="tipo" placeholder="Tipo">
      </div>
      <div class="form-group col-md-6">
        <label class="sr-only" for="">Edad</label>
        <input type="text" class="form-control" id="" name="edad" placeholder="Edad">
      </div>
      <div class="form-group col-md-6">
        <label class="sr-only" for="">Altura</label>
        <input type="text" class="form-control" id="" name="altura" placeholder="Altura">
      </div>
      <input type="file" name="picture" id="picture" class="form-control-file col-md-6">
      <div class="form-group col-md-6">
        <button type="submit" class="btn btn-primary">Guardar</button>
      </div>

    </form>

  </div>

</body>

</html>