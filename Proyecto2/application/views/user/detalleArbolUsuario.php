<?php
defined('BASEPATH') or exit('No direct script access allowed');

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/detalleArbol.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
</head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<title>Document</title>
</head>

<body>
  <div class="container">
    <form method="POST" class="form" role="form" enctype="multipart/form-data">
      <input type="hidden" name="id" value="<?php echo $this->session->usuario->id ?>">
      <br>
      <h1 class="text-center" style="color: #191979;">Detalles del árbol</h1>
      <div class="row">
        <div class="col bg-white border-right">
          <div class="form-group col-md-12">
            <br>
            <h4>Tipo:</h4>
            <input type="text" class="form-control" id="tipo" readonly name="tipo" placeholder="Tipo" value="<?php echo $tree->tipo ?>">
          </div>
          <div class="form-group col-md-12">
            <h4>Nombre:</h4>
            <label class="sr-only" for="">Nombre</label>
            <input type="text" class="form-control" id="" readonly name="nombre" placeholder="Nombre" value="<?php echo $tree->nombre ?>">
          </div>
          <div class="form-group col-md-12">
            <h4>Edad:</h4>
            <label class="sr-only" for="">Edad</label>
            <input type="text" class="form-control" id="" readonly name="edad" placeholder="Edad" value="<?php echo $tree->edad ?>">
          </div>
          <div class="form-group col-md-12">
            <h4>Altura:</h4>
            <label class="sr-only" for="">Altura</label>
            <input type="text" class="form-control" id="" readonly name="altura" placeholder="Altura" value="<?php echo $tree->altura ?>">
          </div>
        </div>
        <div class="col bg-white">
          <div class="form-group col-md-12 text-center">
            <br>
            <img src="/WEB/proyecto2-prograweb/Proyecto2/<?php echo $tree->foto; ?>" width="350" height="350" class="col-md-12"></img>
            <div class="justify-content-center">
              <div class="col-xl-12">
                <div class="card" style="width: 100%">
                  <div class="card-body" id="prueba">
                    <?php
                    if (!empty($fotos)) {
                      foreach ($fotos as $foto) {
                        echo "<img  class='img-fluid' src ='/WEB/proyecto2-prograweb/Proyecto2/{$foto->foto}' width='150' height='150'>";
                      }
                    } else{
                      
                    }
                    ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-12 text-center">
          <br>
          <a href="<?php echo site_url(['user', 'dashboardUsuario/mis-arboles']); ?>" class="btn btn-primary btn-xl">Atrás</a>
        </div>
      </div>
    </form>
  </div>
</body>

</html>