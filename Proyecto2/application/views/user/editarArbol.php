<?php
defined('BASEPATH') or exit('No direct script access allowed');

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" type="text/css" href="detalleArbol.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
</head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<title>Document</title>
</head>

<body>
    <div class="container">
        <form method="POST" action="<?php echo site_url(['arbol', 'editTree/' . $tree->id_arbol]) ?>" id="form1" name="form1" onsubmit="return dimePropiedades()" class="form" role="form" enctype="multipart/form-data">
            <input type="hidden" name="id" value="<?php echo $this->session->usuario->id ?>">
            <br>
            <h1 class="text-center" style="color: #191979;">Editar árbol</h1>
            <div class="row">
                <!--Carga los input con los valores del arbol-->
                <div class="col bg-white border-right">
                    <div class="form-group col-md-12">
                        <br>
                        <label class="sr-only" for="">Tipo</label>
                        <select name="tipos_especie" id="tipos_especie" value="<?php echo $tree->tipo ?>" selected="<?php echo $tree->tipo ?>">
                            <?php
                            if ($especies !== FALSE) {
                                foreach ($especies as $especie)
                                    if ($especie->nombre == $tree->tipo) {
                                        echo '<option selected="true" value="' . $especie->id . '">' . $especie->nombre . '</option>';
                                    } else {
                                        echo '<option value="' . $especie->id . '">' . $especie->nombre . '</option>';
                                    }
                            } else {
                                echo "No hay especies para mostrar";
                            }
                            ?>

                        </select>
                        <input type="hidden" name="especies" />
                    </div>
                    <div class="form-group col-md-12">
                        <label class="sr-only" for="">Edad</label>
                        <input type="text" class="form-control" id="" name="edad" placeholder="Edad" value="<?php echo $tree->edad ?>">
                    </div>
                    <div class="input-group col-md-12">
                        <label class="sr-only" for="">Altura</label>
                        <input type="text" class="form-control" id="" name="altura" placeholder="Altura" value="<?php echo $tree->altura ?>">
                        <div class="input-group-append">
                            <span class="input-group-text" id="addon-wrapping">cm</span>
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <br>
                        <img src="/WEB/proyecto2-prograweb/Proyecto2/<?php echo $tree->foto; ?>" width="300" height="300" class="col-md-12"></img>
                    </div>
                </div>
                <div class="col bg-white">
                    <!--Muestra la galeria de arboles y da la opcion para agregar mas fotos-->
                    <div class="justify-content-center">
                        <div class="col-xl-12">
                            <div class="card">
                                <div class="card-body col-lg-12 col-md-4 col-6">
                                    <?php
                                    if ($gallery !== FALSE) {
                                        foreach ($gallery as $foto) {
                                            echo "<img class='img-fluid' src ='/WEB/proyecto2-prograweb/Proyecto2/{$foto->foto}' width='150' height='150'>";
                                        }
                                    } else {
                                        echo "No hay más imágenes para mostrar";
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-12 text-center">
                        <br>
                        <input type="file" name="upload" id="upload" class="form-control-file col-md-12">
                        <input type="hidden" id="nom_foto" name="nom_foto" class="input__text" value="">
                    </div>
                </div>
            </div>
            <div class="col-md-12 text-center">
                <br>
                <a href="<?php echo site_url(['user', 'dashboardAdministrador']); ?>" class="btn btn-primary btn-md">Atrás</a>
                <button class="btn btn-primary btn-md" type="submit" onclick="ponerNombre()">Actualizar</button>
                <br>
                <br>
                <br>
            </div>
            <script>
                function ponerNombre() {
                    var nom = document.getElementById("upload").value;

                    var filename = nom.replace(/^.*[\\\/]/, '');

                    document.getElementById("nom_foto").value = filename;
                }

                function dimePropiedades() {
                    var indice = document.form1.tipos_especie.selectedIndex;
                    var textoEscogido = document.form1.tipos_especie.options[indice].text;
                    if (textoEscogido == "SELECCIONE") {
                        document.form1.especies.value = '';
                    } else {
                        document.form1.especies.value = textoEscogido;
                    }
                    document.form1.submit();
                }
            </script>
        </form>
    </div>
</body>


</html>