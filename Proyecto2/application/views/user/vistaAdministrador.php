<?php
defined('BASEPATH') or exit('No direct script access allowed');
error_reporting(E_ERROR | E_WARNING | E_PARSE);

if ($this->session->usuario->tipo !== "administrador") {
    redirect(site_url(['user', 'index']));
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/vistaUsuario.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <title>Document</title>
</head>

<body>
    <nav class="navbar" style="background-color: #000080;">
        <!-- Brand -->
        <a class="navbar-brand" href="#">
            <img src="img\¡Amigos de un millón de árboles!.png" width="80" height="80" alt="">
        </a>
        <!-- Links -->
        <ul class="nav ml-auto">
            <li class="nav-item border-right">
                <a class="nav-link" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">Cantidad de amigos registrados</a>
            </li>
            <li class="nav-item border-right">
                <a class="nav-link" data-toggle="collapse" href="#arbolesVendido" aria-expanded="false" aria-controls="arbolesVendido">Cantidad de árboles plantados</a>
            </li>
            <li class="nav-item border-right">
                <a class="nav-link" data-toggle="collapse" href="#registro" aria-expanded="false" aria-controls="registro">Registrar árbol</a>
            </li>
            <li class="nav-item border-right">
                <a class="nav-link" data-toggle="collapse" href="#busquedaA" aria-expanded="false" aria-controls="busquedaA">Administrar árboles</a>
            </li>
            <!--Ventana de perfil-->
            <li class="nav-item dropdown navbar-text">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <span class="far fa-user"></span> 
                    <strong><?php echo $this->session->usuario->nombre; ?></strong>
                    <span class="glyphicon glyphicon-chevron-down"></span>
                </a>
                <div class="container">
                    <ul class="dropdown-menu ml-auto">
                        <li id="login">
                            <div class="navbar-login">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <p class="text-center">
                                            <span><img src="/WEB/proyecto2-prograweb/Proyecto2/<?php echo $this->session->usuario->foto; ?>" width="80" height="80"></img></span>
                                        </p>
                                    </div>
                                    <div class="col-lg-8">
                                        <p class="text-left"><strong><?php echo $this->session->usuario->nombre; ?></strong></p>
                                        <p class="text-left small"><?php echo $this->session->usuario->correo; ?></p>
                                        <p class="text-left">
                                            <a href="<?php echo site_url(['user', 'logout']); ?>" class="btn btn-primary btn-block btn-sm">Cerrar sesión</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <div class="navbar-login navbar-login-session">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <p>
                                            <a href="#" class="btn btn-primary btn-block">Mi perfil</a>
                                            <a href="#" class="btn btn-danger btn-block">Cambiar contraseña</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
    </nav>

    <!--Muestra la tabla de los amigos registrados-->
    <div class="collapse" id="collapseExample">
        <div class="card card-body table-responsive">
            <h3>La cantidad de amigos registrados son: (<?php echo $usersQty; ?>)</h3>
            <table class="table table-light table-hover table-striped">
                <tbody>
                    <tr class="table-success text-center">
                        <td scope="col">Id</td>
                        <td scope="col">Nombre</td>
                        <td scope="col">Apellido</td>
                        <td scope="col">País</td>
                        <td scope="col">Dirección</td>
                        <td scope="col">Correo</td>
                        <td scope="col">Teléfono</td>
                    </tr>
                    <?php
                    foreach ($users as $user) {
                        echo "<tr class='text-center' id='usuario_{$user['id']}'><td>{$user['id']}</td><td>{$user['nombre']}</td>
                    <td>{$user['apellido']}</td><td>{$user['pais']}</td><td>{$user['direccion']}</td><td>{$user['correo']}</td>
                    <td>{$user['telefono']}</td></tr>";
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>

    <!--Muestra el formulario de registro de arboles-->
    <div class="collapse" id="registro">
        <div class="card card-body">
            <form action="<?php echo site_url(['arbol', 'saveTree/']) ?>" id="form1" name="form1" onsubmit="return dimePropiedades()" method="POST" class="form" role="form" enctype="multipart/form-data">
                <div class="row">
                    <div class="col text-center">
                        <img src="<?php echo base_url(); ?>img/cierreagr2.jpg" alt="">
                    </div>
                    <div class="col">
                        <br>
                        <br>
                        <br>
                        <div class="form-group col-md-12 text-center">
                            <h1>Registrar árbol</h1>
                        </div>
                        <div class="alert alert-danger" role="alert">
                            <?php echo validation_errors(); ?>
                        </div>
                        <div class="form-group col-md-12 text-center">
                            <label class="sr-only" for="">Tipo</label>
                            <select name="tipos_especie" value="<?php echo $tree->tipo ?>">
                                <?php
                                if ($especies !== FALSE) {
                                    foreach ($especies as $especie)
                                        echo '<option value="' . $especie->id . '">' . $especie->nombre . '</option>';
                                } else {
                                    echo "No hay especies para mostrar";
                                }
                                ?>
                            </select>
                            <input type="hidden" name="especies" /> 
                        </div>
                        <div class="form-group col-md-12 text-center">
                            <label class="sr-only" for="">Edad</label>
                            <input type="text" class="form-control" id="" name="edad" placeholder="Edad">
                        </div>
                        <div class="input-group mb-2 col-md-12">
                            <input class="form-control" type="text" name="altura" placeholder="Altura">
                            <div class="input-group-append">
                                <span class="input-group-text" id="addon-wrapping">cm</span>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <input type="file" name="upload" id="upload" class="form-control-file col-md-6">
                            <input type="hidden" id="nom_foto" name="nom_foto" class="input__text" value="">
                        </div>
                        <div class="form-group col-md-12 text-center">
                            <button type="submit" class="btn btn-primary md" onclick="ponerNombre()">Guardar</button>
                        </div>
                        <script>
                            function ponerNombre() {
                                var nom = document.getElementById("upload").value;

                                var filename = nom.replace(/^.*[\\\/]/, '');

                                document.getElementById("nom_foto").value = filename;
                            }

                            function dimePropiedades() {
                                var indice = document.form1.tipos_especie.selectedIndex;
                                var textoEscogido = document.form1.tipos_especie.options[indice].text;
                                if (textoEscogido == "SELECCIONE") {
                                    document.form1.especies.value = '';
                                } else {
                                    document.form1.especies.value = textoEscogido;
                                }
                                document.form1.submit();
                            }
                        </script>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <!--Muestra las opciones del nombre que el administrador ingreso para ver los arboles-->
    <div class="collapse" id="busquedaA">
        <div class="card card-body">
            <form method="GET" action="<?php echo site_url(['user', 'searchByName']) ?>">
                <input id="buscar" name="buscar" type="search" placeholder="Buscar aquí…" autofocus onkeyup="<?php echo site_url(['user', 'searchByName']) ?>">
                <input type="submit" class="btn btn-primary md" value="buscar">
            </form>
            <br>
            <section id="tabla_resultado">
                <table class="table table-light table-hover table-striped">
                    <tbody>
                        <tr class="table-success text-center">
                            <td scope="col">Id</td>
                            <td scope="col">Nombre</td>
                            <td scope="col">Apellido</td>
                            <td scope="col">País</td>
                            <td scope="col">Dirección</td>
                            <td scope="col">Correo</td>
                            <td scope="col">Teléfono</td>
                            <td scope="col">Acciones</td>
                        </tr>
                        <?php
                        if ($result) {
                            foreach ($result->result_array() as $user) {
                                echo "<tr class='text-center' id='student_{$user['id']}'><td>{$user['id']}</td><td>{$user['nombre']}</td>
                            <td>{$user['apellido']}</td><td>{$user['pais']}</td><td>{$user['direccion']}</td><td>{$user['correo']}</td>
                            <td>{$user['telefono']}</td><td><a href=" . site_url('/arbol/viewTreeByFriend/' . $user['id']) . ">Ver arboles</a>";
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </section>
        </div>
    </div>

    <div class="collapse" id="arbolesVendido">
        <div class="card card-body table-responsive">
            <h3>La cantidad de árboles vendidos son: (<?php echo $treesQty; ?>)</h3>
            <table class="table table-light table-hover table-striped">
                <tbody>
                    <tr class="table-success text-center">
                        <td scope="col">Id</td>
                        <td scope="col">Nombre</td>
                        <td scope="col">Apellido</td>
                        <td scope="col">Edad del árbol</td>
                        <td scope="col">Altura</td>
                        <td scope="col">Especie</td>
                        <td scope="col">Donación</td>
                    </tr>
                    <?php
                    if ($arbolVendido !== FALSE) {
                        foreach ($arbolVendido as $arbol) {
                            echo "<tr class='text-center' id='arbol_{$arbol['id_arbol']}'><td>{$arbol['id_arbol']}</td><td>{$arbol['nombre']}</td>
                    <td>{$arbol['apellido']}</td><td>{$arbol['edad']}</td><td>{$arbol['altura']}</td><td>{$arbol['tipo']}</td>
                    <td>{$arbol['donacion']}</td></tr>";
                        }
                    } else {
                        echo "no";
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</body>



</html>