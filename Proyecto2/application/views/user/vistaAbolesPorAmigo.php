<?php
defined('BASEPATH') or exit('No direct script access allowed');

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" type="text/css" href="detalleArbol.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
</head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<title>Document</title>
</head>

<body>
  <div class="container">
    <h1 class="font-weight-light text-center text-lg-left mt-4 mb-0">Árboles del usuario</h1>
    <div class="row  h-100 justify-content-start">
      <div class="col-sm-12">
        <div class="card justify-content-start">
          <div class="card-body justify-content-start">
            <?php
            if ($mytrees !== FALSE) {
              foreach ($mytrees as $tree) {
                echo "<img src ='/WEB/proyecto2-prograweb/Proyecto2/{$tree->foto}' width='150' height='150' >
                <h3>{$tree->nombre}</h3><p><a href=" . site_url('/arbol/loadEditView/' . $tree->id_arbol) . ">Editar arbol</a></p>";
              }
            } else {
              echo "No hay arboles para mostrar";
            }

            ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>

</html>