<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Arbol extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('image_lib');
    }

    /**
     * Le muestra informacion al amigo de su arbol.
     */
    public function detalleArbolUsuario($id)
    {
        $data['tree'] = $this->Arbol_model->getTree($id)[0];
        $data['fotos'] = $this->Arbol_model->obtenerGaleriaPorArbol($id);
        $this->load->view('user/detalleArbolUsuario', $data);
    }

    /**
     * Muestra los arboles de un amigo por el id
     */
    public function viewTreeByFriend($id)
    {
        $data['mytrees'] = $this->Arbol_model->getMyTrees($id);
        $this->load->view('user/vistaAbolesPorAmigo', $data);
    }

    /**
     * Metodo para cargar la vista para editar el arbol
     */
    public function loadEditView($id)
    {
        //Carga los datos de tipos de especie de arboles.
        $data['especies'] = $this->Arbol_model->getKindTree();
        //Obtiene un arbol por id
        $data['tree'] = $this->Arbol_model->getTree($id)[0];
        //Carga las fotos que se van a mostrar en la galeria
        $data['gallery'] = $this->Arbol_model->obtenerGaleriaPorArbol($id);
        $this->load->view('user/editarArbol', $data);
    }


    /**
     * Metodo para que el administrador pueda actualizar el arbol.
     */
    public function editTree($id)
    {
        $tipo = $this->input->post('especies');
        $edad = $this->input->post('edad');
        $altura = $this->input->post('altura');
        $img = "update/" . $this->input->post('nom_foto');
        $this->load_file();
        //Actualiza los detalles de edad, altura y tipo
        $updateDatos = $this->Arbol_model->updateDetalles($tipo, $edad, $altura, $id);
        $updateFotos = $this->Arbol_model->insertarImagenesGaleria($id, $img);
        if ($updateDatos || $updateFotos) {
            echo "Ha ocurrido un error";
        } else {
            redirect(site_url(['arbol', 'loadEditView/' . $id]));
        }
    }



    /**
     * Carga los arboles por id en la vista detalleArbol
     */
    public function detalleArbol($id)
    {
        $data['tree'] = $this->Arbol_model->getTree($id)[0];
        $this->load->view('user/detalleArbol', $data);
    }

    /**
     * Guarda un arbol en la base de datos.
     */
    public function saveTree()
    {
        $this->form_validation->set_rules('nom_foto', 'Foto', 'required');
        $this->form_validation->set_rules('edad', 'Edad', 'required');
        $this->form_validation->set_rules('altura', 'Altura', 'required');
        // check the database with that information
        if ($this->form_validation->run() == FALSE) {
            redirect(site_url(['user', 'dashboardAdministrador/#registro']));
        } else {
            $img = "update/" . $this->input->post('nom_foto');
            $tipo = $this->input->post('especies');
            $edad = $this->input->post('edad');
            $altura = $this->input->post('altura');
            $this->load_file();
            $register = $this->Arbol_model->saveTree($img, $tipo, $edad, $altura);
            // return error or redirect to landing page
            if ($register) {
                echo "Ha ocurrido un error al registrar el arbol";
            } else {
                redirect(site_url(['user', 'dashboardAdministrador']));
            }
        }
    }

    /**
     * Metodo para cargar una imagen en la carpeta de update
     */
    function load_file()
    {

        $mi_archivo = 'upload';
        $config['upload_path'] = "update/";
        $config['allowed_types'] = "jpg|png";
        $config['max_size'] = "5000";
        $config['max_width'] = "2000";
        $config['max_height'] = "2000";

        $this->load->library('upload', $config);
        if (!$this->upload->do_upload($mi_archivo)) {
			//*** ocurrio un error
			echo $this->upload->display_errors();
			return;
		}

		var_dump($this->upload->data());
    }

    /**
     * Metodo para asignarle los datos al arbol cuando el dueno lo compra
     */
    public function updateTree($id)
    {
        $idPropietario = $this->session->usuario->id;
        $nombre = $this->input->post('nombreArbol');
        $donacion = $this->input->post('donacion');
        // check the database with that information
        $update = $this->Arbol_model->updateArbol($idPropietario, $nombre, $donacion, $id);
        // return error or redirect to landing page
        if ($update) {
            echo "Ha ocurrido un error al comprar el arbol";
        } else {
            redirect(site_url(['user', 'dashboardUsuario']));
        }
    }
}
