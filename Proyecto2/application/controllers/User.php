<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url', 'form');
		$this->load->library('form_validation');
		$this->load->library('image_lib');
	}


	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function login()
	{
		// params
		// load data
		$this->load->view('user/login');
	}

	/**
	 * Metodo para cerrar sesion y redireccionar al index
	 */
	public function logout()
	{
		$this->session->sess_destroy();
		$this->session->set_flashdata('error', 'Inicie sesión nuevamente');
		redirect(site_url(['user', 'index']));
	}

	/**
	 * Metodo para cargar la vista del usuario
	 */
	public function dashboardUsuario()
	{
		$id = $this->session->usuario->id;
		//Obtiene los datos de los arboles de usuario por id.
		$data['mytrees'] = $this->Arbol_model->getMyTrees($id);
		//Carga los arboles disponibles para que el usuario los pueda comprar.
		$data['arboles'] = $this->Arbol_model->getTrees();
		$this->load->view('user/vistaUsuario', $data);
	}

	/**
	 * Metodo para cargar la vista del administrador.
	 */
	public function dashboardAdministrador()
	{
		//Carga la informacion de los arboles vendidos.
		$data['arbolVendido'] = $this->Arbol_model->informacionArbolesVendidos();
		//Obtiene la cantidad de arboles vendidos
		$data['treesQty'] = sizeof($data['arbolVendido']);
		//Carga los tipos de especies de arboles.
		$data['especies'] = $this->Arbol_model->getKindTree();
		//Carga todos los amigos registrados
		$data['users'] = $this->User_model->getUsers();
		//Muestra la cantidad de amigos registrados
		$data['usersQty'] = sizeof($data['users']);
		$this->load->view('user/vistaAdministrador', $data);
	}


	/**
	 * Busca un usuario por el nombre
	 */
	public function searchByName()
	{
		$data = array();
		$search = $this->input->get('buscar', TRUE);
		if($search){
			$result = $this->User_model->getFriendbyName(trim($search));
			if($result != false){
				$data = array('result' => $result);
			}else{
				$data = array('result' => '');
			}
		}else{
			$data = array('result' => '');
		}
		$this->load->view('user/vistaAdministrador', $data);
	}

	/**
	 * Carga la vista de registrar usuarios
	 */
	public function register()
	{
		$this->load->view('user/register');
	}

	/**
	 * Carga la vista index
	 */
	public function index()
	{
		$this->load->view('user/index');
	}

	/**
	 * Carga la vista detalle arbol
	 */
	public function detalleArbol()
	{
		$this->load->view('user/detalleArbol');
	}


	/**
	 * Authenticates a user
	 */
	public function authenticate()
	{
		// get username and password
		$pass = $this->input->post('contrasena');
		$email = $this->input->post('correo');

		// check the database with that information
		$authUser = $this->User_model->authenticate($email, $pass);
		// return error or redirect to landing page
		if ($authUser) {
			$this->session->set_userdata('usuario', $authUser);
			$tipo = $this->session->usuario->tipo;
			if ($tipo == 'usuario') {
				redirect(site_url(['user', 'dashboardUsuario']));
			} elseif ($tipo == 'administrador') {
				redirect(site_url(['user', 'dashboardAdministrador']));
			}
		} else {
			$this->session->set_flashdata('error', 'Invalid user name or password');
			redirect(site_url(['user', 'index']));
		}
	}

	/**
	 * Metodo para registrar un usuario
	 */
	public function validateRegister()
	{
		$this->form_validation->set_rules('nombre', 'Nombre', 'required');
		$this->form_validation->set_rules('apellido', 'Apellido', 'required');
		$this->form_validation->set_rules('pais', 'Pais', 'required');
		$this->form_validation->set_rules('telefono', 'Telefono', 'required|numeric');
		$this->form_validation->set_rules('direccion', 'Direccion', 'required');
		$this->form_validation->set_rules('correo', 'Correo', 'required|valid_email|is_unique[usuario.correo]');
		$this->form_validation->set_rules('contrasena', 'Contrasena', 'required');
		// check the database with that information
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('user/register');
		} else {
			$foto = "update/".$this->input->post('nom_arbol');
			$nombre = $this->input->post('nombre');
			$apellido = $this->input->post('apellido');
			$pais = $this->input->post('pais');
			$telefono = $this->input->post('telefono');
			$direccion = $this->input->post('direccion');
			$correo = $this->input->post('correo');
			$contrasena = $this->input->post('contrasena');
			$tipo = "usuario";
			$this->cargar_archivo();
			$register = $this->User_model->register($foto, $nombre, $apellido, $pais, $telefono, $direccion, $correo, $contrasena, $tipo);
			// return error or redirect to landing page
			if ($register) {
				echo "Ha ocurrido un error al registrarse";
			} else {
				redirect(site_url(['user', 'index']));
			}
		}
	}

	/**
	 * Metodo para cargar una imagen en una carpeta
	 */
	function cargar_archivo()
	{

		$foto = $this->input->post('nom_arbol');
		$mi_archivo = 'upload';
		$config['upload_path'] = "update/";
		$config['allowed_types'] = "jpg|png";
		$config['max_size'] = "5000";
		$config['max_width'] = "2000";
		$config['max_height'] = "2000";

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload($mi_archivo)) {
			//*** ocurrio un error
			echo $this->upload->display_errors();
			return;
		}

		var_dump($this->upload->data());
	}
}
