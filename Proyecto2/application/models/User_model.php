<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User_model extends CI_Model
{


  /**
   *  Valida si un usuario existe en la base de datos
   *
   * @param $username  The username
   * @param $password The user's password
   */
  public function authenticate($email, $password)
  {
    $query = $this->db->get_where('usuario', array('correo' => $email, 'contrasena' => $password));
    if ($query->result()) {
      return $query->result()[0];
    } else {
      return false;
    }
  }

  /**
   *  Registra un usuario en la base de datos
   *
   * @param $nombre  Nombre del usuario
   * @param $apellido Apellido del usuario
   * @param $pais Pais de donde vive el usuario
   * @param $telefono Telefono del usuario
   * @param $direccion Direccion del usuario
   * @param $correo Correo del usuario
   * @param $contrasena Contrasena del usuario
   * @param $foto Foto del usuario
   */
  public function register($foto, $nombre, $apellido, $pais, $telefono, $direccion, $correo, $contrasena, $tipo)
  {
    $data  =  array(
      'foto'  =>  $foto,
      'nombre'  =>  $nombre,
      'apellido'  =>  $apellido,
      'pais'  =>  $pais,
      'telefono'  =>  $telefono,
      'direccion'  =>  $direccion,
      'correo'  =>  $correo,
      'contrasena'  =>  $contrasena,
      'tipo'  =>  $tipo,
    );
    $query = $this->db->insert('usuario',  $data);
  }

  /**
   *  Obtiene los usuarios por ID
   *
   * @param $id  El ID del usuario
   */
  public function getUser($id)
  {
    $query = $this->db->get_where('usuario', array('id' => $id));
    if ($query->result()) {
      return $query->result();
    } else {
      return false;
    }
  }

  /**
   *  Obtiene todos los usuarios de la base de datos
   *
   */
  public function getUsers()
  {
    $this->db->where('tipo !=', "administrador");
    $query = $this->db->get('usuario');
    if (!$query->num_rows() == 1) {
      return false;
    }
    return $query->result_array();
  }


  /**
   *  Hace una busqueda de los usuarios con las letras que el usuario vaya digitanto
   *
   * @param $busqueda  El nombre que el administrador va a ir digitando
   */
  public function getFriendbyName($busqueda)
  {
    
    $this->db->like('nombre', $busqueda);
    $this->db->or_like('apellido', $busqueda);
    $query = $this->db->get('usuario');
    if ($query->num_rows() > 0) {
      return $query;
    } else {
      return false;
    }
  }
}
