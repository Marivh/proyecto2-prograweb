<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Arbol_model extends CI_Model
{

    /**
     *  Registra un usuario en la base de datos
     *
     * @param $foto  Foto del arbol
     * @param $apellido Especie del arbol
     * @param $edad Edad del arbol
     * @param $altura Altura del arbol
     */
    public function saveTree($img, $tipo, $edad, $altura)
    {
        $data  =  array(
            'foto'  =>  $img,
            'tipo'  =>  $tipo,
            'edad'  =>  $edad,
            'altura'  =>  $altura
        );
        $query = $this->db->insert('arbol',  $data);
        var_dump($data);
    }

    public function updateArbol($idPropietario, $nombre, $donacion, $id)
    {
        $data  =  array(
            'id_propietario'  =>  $idPropietario,
            'nombre'  =>  $nombre,
            'donacion'  =>  $donacion
        );

        $this->db->where('id_arbol',  $id);
        $this->db->update('arbol', $data);
    }

    /**
     * Metodo para actualizar los detalles del arbol por parte del administrador
     */
    function updateDetalles($tipo, $edad, $altura, $id)
    {
        $data  =  array(
            'tipo'  =>  $tipo,
            'edad'  =>  $edad,
            'altura'  =>  $altura
        );
        $this->db->where('id_arbol',  $id);
        $this->db->update('arbol', $data);
    }



    /**
     *  Registra un usuario en la base de datos
     *
     * @param $foto  Foto del arbol
     * @param $apellido Especie del arbol
     * @param $edad Edad del arbol
     * @param $altura Altura del arbol
     */
    public function insertarImagenesGaleria($id, $foto)
    {
        $data  =  array(
            'id_arbol'  =>  $id,
            'foto'  =>  $foto,
        );
        $query = $this->db->insert('galeria',  $data);
    }

    /**
     *  Obtiene los usuarios por ID
     *
     * @param $id  El ID del usuario
     */
    public function getTree($id)
    {
        $query = $this->db->get_where('arbol', array('id_arbol' => $id));
        if ($query->result()) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     *  Obtiene los usuarios por ID
     *
     * @param $id  El ID del usuario
     */
    public function getMyTrees($id)
    {
        $query = $this->db->get_where('arbol', array('id_propietario' => $id));
        if ($query->result()) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     *  Obtiene todos los arboles de la base de datos
     *
     */
    public function getTrees()
    {
        $query = $this->db->select('*');
        $query = $this->db->where('id_propietario', NULL, FALSE);
        $query = $this->db->get('arbol');
        if ($query->result()) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     *  Obtiene los tipos de arboles registrados
     *
     */
    public function getKindTree()
    {
        $this->db->order_by('nombre' ,  'ASC'); 
        $query = $this->db->get('especie');
        if ($query->result()) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**Obtiene de la base de datos los datos del arbol por amigo */
    function getTreeByOwn($nombreAmigo)
    {
        $query = " select a.id_arbol, a.foto, a.tipo, a.edad, a.altura, a.nombre from arbol as a, usuario as u
      WHERE a.id_propietario = u.id and u.nombre = '%$nombreAmigo%';";
        if ($query->num_rows() > 0) {
            return $query;
        } else {
            return false;
        }
    }

    //Metodo para obtener los datos del arbol por amigo//
    function obtenerGaleriaPorArbol($idArbol)
    {
        $this->db->select('g.foto');
        $this->db->from('arbol as a');
        $this->db->join('galeria as g',  'a.id_arbol = g.id_arbol');
        $this->db->where('a.id_arbol',  $idArbol);
        $query  =  $this->db->get();
        if ($query->result()) {
            return $query->result();
        } else {
            return false;
        }
    }
    /**
     * Muestra la informacion del arbol y del dueno de este
     */
    function informacionArbolesVendidos()
    {
        $this->db->select('a.id_arbol, u.nombre, u.apellido, a.tipo, a.edad, a.altura, a.foto, a.donacion ');
        $this->db->from('arbol a');
        $this->db->join('usuario u', 'a.id_propietario = u.id');
        $aResult = $this->db->get();

        if (!$aResult->num_rows() == 1) {
            return false;
        }
        return $aResult->result_array();
    }
}
